#include <Arduino.h>

#define wheel_speed_pin 2         // Geschwindigeitssensor auf Pin 2 anschließen
#define pedal_speed_pin 0         // Tretsensor auf Pin 3 anschließen
#define Motor_Ein 4               // Bremse auf Pin 4 anschließen (-> Ausgang: wenn High, dann Motor ein)
#define wheel_circumference 2165  // (in mm)  (fuer den Anfang angenommen)
#define min_pedalFreq 25          // (in 1/min) Mindestfrequenz des Pedalieren 
#define pulses_per_rotation 4     // Bei einer Pedalumdrehung werden x Impulse an den Controller geschickt
#define speed_limit 25            // (in km/h) Begrenzung, bis der Motor unterstützen darf
#define Anfahrhilfe 6             // (in km/h)

// Variablen

volatile unsigned int wheel_timer;
volatile unsigned int wheel_counter;
volatile unsigned long i = 0;
volatile unsigned long j = 0;
float Zeit_pro_Takt = 0.062;                                                 // (in Millisekunden) (Prescaler/Taktfrequenz = 1024/16,5Mhz)
float speed = 0;                                                             // (in km/h)
float speed_temp = 0;                                                        // (in km/h)
float pedalFreq = 0;                                                         // (in 1/min)
float pedalFreq_temp = 0;                                                    // (in 1/min)
float max_pedal_pause = (60.0 / min_pedalFreq) / pulses_per_rotation;        // (in Sekunden) Maximalwert der Pause zwischen den steigenden Flanken
float live_pedal_pause = 0;                                                  // (in Sekunden) IST-Wert der Pause zwischen den steigenden Flanken
float max_wheel_pause = 5;                                                   // (in Sekunden) Maximalwert der Pause zwischen den steigenden Flanken
float live_wheel_pause = 0;                                                  // (in Sekunden) IST-Wert der Pause zwischen den steigenden Flanken



int main(void)
{
cli(); // alle Interrupts sperren

//####################################################################
  // Geschwindigkeitssensor -> Timer 0
  //pinMode(wheel_speed_pin, INPUT);
  pinMode(wheel_speed_pin, INPUT_PULLUP);     // Pullups für Simulation mit Tastern
  GIMSK = GIMSK | (1 << INT0);                // Pin Change Interrupt Enable
  MCUCR = MCUCR | (1<<ISC01) | (1<<ISC00);    // Interrupt Int0 wird auf steigende Flanke am Pin 2 ausgeloest

  TCCR0B = TCCR0B | (1<<CS00) | (1<<CS02);    // Timer-Start / Teiler 1024
  TIMSK = TIMSK | (1<<TOIE0);                 // Timer-Overflow-Interrupt erlauben


//####################################################################
  // Tretsensor -> Timer 1
  //pinMode(pedal_speed_pin, INPUT);
  //pinMode(pedal_speed_pin, INPUT_PULLUP);     // Pullups für Simulation mit Tastern
  // ACSR = ACSR | (1<<ACIE) | (1<<ACIS0) | (1<<ACIS1);  // Analog Comparator Interrupt auf steigende Flanke erlauben
  GIMSK = GIMSK | (1 << PCIE);                // Pin Change Interrupt Enable
  PCMSK = PCMSK | (1 << PCINT0);              // Pin 3 als Pin Change Interrupt setzen

  TCCR1 = TCCR1 | (1<<CS13) | (1<<CS11) | (1<<CS10);      // Timer-Start / Teiler 1024
  TIMSK = TIMSK | (1<<TOIE1);                 // Timer-Overflow-Interrupt erlauben


//####################################################################
  // Motor ansteuern
  pinMode(Motor_Ein, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);    // als Statusanzeige: 1 = Motorunterstützung an
  digitalWrite(Motor_Ein, HIGH);   // Motor soll im Ruhezustand immer angesteuert werden können
  digitalWrite(LED_BUILTIN, HIGH);

sei();  // alle Interrupts erlauben

  while (1) 
  {
    speed_temp = 3.6 * (wheel_circumference) / ((wheel_timer + wheel_counter * 256) * Zeit_pro_Takt); // 3.6 ist für Umrechnung von mm/ms in km/h
    live_wheel_pause = ((TCNT0 + i * 256) * Zeit_pro_Takt) / 1000;
    if(speed_temp < 40){
      speed = speed_temp;
    }
    live_pedal_pause = ((TCNT1 + j * 256) * Zeit_pro_Takt) / 1000;

    if(speed == 0 || speed > speed_limit || live_wheel_pause > max_wheel_pause || (speed > Anfahrhilfe && (live_pedal_pause > max_pedal_pause))){
      digitalWrite(Motor_Ein, LOW);
      digitalWrite(LED_BUILTIN, LOW);
    } 
    else {
      digitalWrite(Motor_Ein, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }
}


ISR(INT0_vect) // Geschwindigkeitssensor-Signal
{
  wheel_timer = TCNT0;
  wheel_counter = i;
  //Timer und Zaehler auf Null zuruecksetzen
  TCNT0, i = 0;
}

ISR(PCINT0_vect) // Tretsensor-Signal
{
  //Timer und Zaehler auf Null zuruecksetzen
  TCNT1, j = 0;
}

ISR(TIMER0_OVF_vect)
{
  i++;
}

ISR(TIMER1_OVF_vect)
{
  j++;
}