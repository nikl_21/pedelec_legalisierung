#ifndef LEGALPEDELEC_H
#define LEGALPEDELEC_H

#include <Arduino.h>
#include <avr/sleep.h>

#if defined(__AVR_ATmega328P__)

const float Zeit_pro_Takt = 0.064;                  // (in Millisekunden) (Prescaler/Taktfrequenz = 128*8/16Mhz = 1024/16MHz)
const int max_timer_value = 256;                    // bei 8-Bit-Timer

//für Umrechnungen:
const float sec_per_min = 60.0;
const float Umrechnung_speed = 3.6;                 // 3.6 ist für Umrechnung von mm/ms in km/h
const int ms_per_s = 1000;


class LegalPedelec
{
    //Attribute
    private:
        int Motor_Ein;
        int wheel_circumference;
        int pulses_per_rotation;
        int speed_limit;
        int Anfahrhilfe;
        float max_wheel_pause;                       // (in Sekunden) Maximalwert der Pause zwischen den steigenden Flanken
        float max_pedal_pause;                       // (in Sekunden) Maximalwert der Pause zwischen den steigenden Flanken
        float speed = 0;                             // (in km/h)
        float live_pedal_pause;                      // (in Sekunden) IST-Wert der Pause zwischen den steigenden Flanken: Tretsensor
        float live_wheel_pause;                      // (in Sekunden) IST-Wert der Pause zwischen den steigenden Flanken: Geschwindigkeitssensor

    //Konstruktor
    public:
        LegalPedelec(int w_c, int min_pedalFreq, int p_per_r, int s_l, float max_w, int A);

    //Methoden
    public:
        void init_(bool AC, int Mo_E, bool PWR_Saving);
        void abfrage(volatile unsigned int wheel_timer, volatile unsigned int wheel_counter, volatile unsigned long i, volatile unsigned long j);
        void debug_init();
        void debug_abfrage();
        void pwr_save(int power_saving_delay);
};

#else

    #error "Funktioniert nur mit dem Arduino Nano!"

#endif

#endif