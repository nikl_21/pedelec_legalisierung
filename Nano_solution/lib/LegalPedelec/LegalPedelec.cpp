#include "LegalPedelec.h"

// #######################################################################
// Konstruktor (Definition der Spezifikationen des Pedelecs und der Grenzwerte)
/*
    wheel_circumference      (in mm)  entspricht dem Umfang des Rades
    min_pedalFreq            (in 1/min) Mindestfrequenz des Pedalieren 
    pulses_per_rotation      Bei einer Pedalumdrehung werden x Impulse an den Controller geschickt
    speed_limit              (in km/h) Begrenzung, bis der Motor unterstützen darf
    max_wheel_pause          (in s) maximale Zeit, die der Geschwindigkeitssensor kein Signal liefern darf, bis Motor abschaltet
    Anfahrhilfe              (in km/h)

*/

LegalPedelec::LegalPedelec(int w_c, int min_pedalFreq, int p_per_r, int s_l, float max_w, int A){
    wheel_circumference = w_c;
    pulses_per_rotation = p_per_r;
    speed_limit = s_l;
    max_wheel_pause = max_w;
    Anfahrhilfe = A;
    max_pedal_pause = (sec_per_min / min_pedalFreq) / pulses_per_rotation;        // (in Sekunden) Maximalwert der Pause zwischen den steigenden Flanken
}

// #######################################################################
// init
/* 
    Geschwindigeitssensor auf INT0 (PIN D2) anschließen
    Tretsensor auf INT1 (PIN D3) anschließen oder Analog Comparator verwenden (true) (+ -> PIN D6) (- -> PIN D7)
    Bremse auf Motor_Ein anschließen (-> Ausgang: wenn High, dann Motor ein)
    Freigabe zum Strom sparen setzen! (dann wird Systemtakt verringert, aber das Debugging kann nicht verwendet werden)
*/

void LegalPedelec::init_(bool AC, int Mo_E, bool PWR_Saving){      // für Analog Comparator
    Motor_Ein = Mo_E;
    pinMode(Mo_E, OUTPUT);

    cli(); // alle Interrupts sperren

    //Timer0/INT0 initialisieren
    EIMSK = EIMSK | (1<<INT0);                  // External Interrupt 0 Enable
    EICRA = EICRA | (1<<ISC01) | (1<<ISC00);    // Interrupt Int0 wird auf steigende Flanke am Pin 2 ausgeloest
    TIMSK0 = TIMSK0 | (1<<TOIE0);               // Timer-Overflow-Interrupt erlauben
    
    //Timer2/AC bzw. INT1 initialisieren
    TIMSK2 = TIMSK2 | (1<<TOIE2);               // Timer-Overflow-Interrupt erlauben

    if(AC){
        //AC initialisieren
        ACSR = ACSR | (1<<ACIE) | (1<<ACIS0) | (1<<ACIS1);  // Analog Comparator Interrupt auf steigende Flanke erlauben
    }
    else{
        //INT1 initialisieren
        EIMSK = EIMSK | (1<<INT1);                              // External Interrupt 1 Enable
        EICRA = EICRA | (1<<ISC11) | (1<<ISC10);                // Interrupt Int1 wird auf steigende Flanke am Pin 3 ausgeloest
    }
    
    //Prescaler für Timer0/Timer2
    if(PWR_Saving){
        CLKPR = 0b10000000;
        CLKPR = 0b00000111;                         // Systemtakt reduzieren auf 125kHz (Teiler 128)
        TCCR0B = TCCR0B | (1<<CS01);                // Timer0-Start / Teiler 8
        TCCR2B = TCCR2B | (1<<CS21);                // Timer2-Start / Teiler 8
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);        // Sleep-Moodus "Power Down" setzen
    }
    else{
        TCCR0B = TCCR0B | (1<<CS00) | (1<<CS02);                // Timer0-Start / Teiler 1024
        TCCR2B = TCCR2B | (1<<CS22) | (1<<CS21) | (1<<CS20);    // Timer2-Start / Teiler 1024
    }

    sei();  // alle Interrupts erlauben
}

// #######################################################################
// abfrage
/*
    es müssen alle Parameter, die in den ISRs verändert werden, dieser Methode übergeben werden
*/

void LegalPedelec::abfrage(volatile unsigned int wheel_timer, volatile unsigned int wheel_counter, volatile unsigned long i, volatile unsigned long j){
    speed = Umrechnung_speed * (wheel_circumference) / ((wheel_timer + wheel_counter * max_timer_value) * Zeit_pro_Takt); 
    live_wheel_pause = ((TCNT0 + i * max_timer_value) * Zeit_pro_Takt) / ms_per_s;
    live_pedal_pause = ((TCNT2 + j * max_timer_value) * Zeit_pro_Takt) / ms_per_s;

    if(speed == 0 || speed > speed_limit || live_wheel_pause > max_wheel_pause || (speed > Anfahrhilfe && live_pedal_pause > max_pedal_pause)){
      digitalWrite(Motor_Ein, LOW);
      digitalWrite(LED_BUILTIN, LOW);
    } 
    else {
      digitalWrite(Motor_Ein, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
    }   
}

// #######################################################################
// debug_init
/*
    Debugging mit dem seriellen Monitor funktioniert nur, wenn bei init_ nicht der Stromsparmodus ausgewählt wurde!
*/

void LegalPedelec::debug_init(){
    Serial.begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);    // als Statusanzeige: 1 = Motorunterstützung an
    Serial.println("Setup ausgeführt.");
}

// #######################################################################
// debug_abfrage
/*
    wiederholende Ausgabe der Parameter auf dem seriellen Monitor 
*/

void LegalPedelec::debug_abfrage(){
    Serial.print(speed);
    Serial.print(" km/h\t");
    Serial.print(live_wheel_pause);
    Serial.print(" s\t\t");
    Serial.print(live_pedal_pause);
    Serial.print(" s\t\t");
    Serial.print("Motor: ");
    Serial.println(digitalRead(Motor_Ein));
}

// #######################################################################
// pwr_save_abfrage
/*
    power_saving_delay       (in Sekunden) nach welcher Zeit der µC in den Stromsparmodus gehen soll, wenn kein Geschwindigkeitssignal kommt

*/

void LegalPedelec::pwr_save(int power_saving_delay){
    if(live_wheel_pause > power_saving_delay){          // Stromsparmodus
      ACSR = ACSR & ~(1<<ACIE);
      ACSR = ACSR | (1<<ACD);                           // Analog Comparator deaktivieren
      sleep_mode();                                     // Sleep Modus aktivieren
    }
    if(ACSR & (1<<ACD)){
      ACSR = ACSR & ~(1<<ACD);
      ACSR = ACSR | (1<<ACIE);                          // Analog Comparator aktivieren
    }
}