#include <LegalPedelec.h>

volatile unsigned int wheel_timer;
volatile unsigned int wheel_counter;
volatile unsigned long i = 0;
volatile unsigned long j = 0;

int main(void)
{
  LegalPedelec Bike_KTM(2165, 25, 4, 25, 30, 6);
  Bike_KTM.init_(true, 4, true);
  //Bike_KTM.debug_init();
   
  while (1) 
  {
    Bike_KTM.abfrage(wheel_timer, wheel_counter, i, j);
    Bike_KTM.pwr_save(60);
    //Bike_KTM.debug_abfrage();
  }
}

ISR(INT0_vect) // Geschwindigkeitssensor-Signal
{
    wheel_timer = TCNT0;
    wheel_counter = i;
    //Timer und Zaehler auf Null zuruecksetzen
    TCNT0, i = 0;
}

ISR(ANALOG_COMP_vect) // Tretsensor-Signal
{
    //Timer und Zaehler auf Null zuruecksetzen
    TCNT2, j = 0;
}

ISR(TIMER0_OVF_vect)
{
    i++;
}

ISR(TIMER2_OVF_vect)
{
    j++;
}

// ISR für den Fall, dass man nicht den AC-Modus auswählt
ISR(INT1_vect) // Tretsensor-Signal
{
    //Timer und Zaehler auf Null zuruecksetzen
    TCNT2, j = 0;
}